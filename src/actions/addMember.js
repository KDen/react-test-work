import { ADD_MEMBER } from './types'

const addMember = (id, date, name, email, phone, distance, payment) => (dispatch) => {
  dispatch({
    type: ADD_MEMBER,
    payload: {
      id, date, name, email, phone, distance, payment
    }
  });
};

export { addMember };