import { ADD_MEMBERS_LIST } from './types'

const addMembers = (membersList) => (dispatch) => {
  dispatch({
    type: ADD_MEMBERS_LIST,
    payload: { membersList }
  });
};

export { addMembers };