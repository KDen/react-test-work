import React from "react";
import { Provider } from 'react-redux';
import store from '../store';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import MainPage from "../Pages/MainPage";

function App() {
  return (
    <>
      <Provider store={store}>
        <MainPage/>
      </Provider>
    </>
  );
}

export default App;