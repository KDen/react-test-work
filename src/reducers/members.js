import {ADD_MEMBER, ADD_MEMBERS_LIST} from '../actions/types';

const initialState = [];

const reducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch(type) {
    case ADD_MEMBER:
      return [...state, payload];

    case ADD_MEMBERS_LIST:
      return Object.assign(...state, payload.membersList);

    default:
      return state;
  }
};

export default reducer;