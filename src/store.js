import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
// import logger from 'redux-logger'
import rootReducer from './reducers';

const initialState = {};

const store = createStore(rootReducer, initialState, compose(
  // applyMiddleware(thunk, logger),
  applyMiddleware(thunk)
));

export default store;