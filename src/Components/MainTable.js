import React, {Component} from 'react';
import {Table} from "react-bootstrap";

class MainTable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      members: this.props.membersList
    };
  }

  renderTableRows() {
    this.props.sortTable();
    return this.props.membersList.map(item => {
      return (<tr key={item.id}>
        <td className="">{item.name}</td>
        <td className="">{item.date}</td>
        <td className="" style={{wordBreak: "break-all"}}>
          {item.email}
        </td>
        <td className="">{item.phone}</td>
        <td className="">{item.distance}</td>
        <td className="">{item.payment}</td>
        <td className="">{item.dateRegistration}</td>
      </tr>)
    });
  }

  render() {
    return (
      <>
        <Table size="sm">
          <thead>
            <tr>
              <th className="align-top">ФИО участника</th>
              <th className="align-top">Дата рождения</th>
              <th className="align-top">Email</th>
              <th className="align-top">Телефон</th>
              <th className="align-top">Дистанция, км</th>
              <th className="align-top">Сумма взноса, руб.</th>
              <th className="align-top">Дата регистрации</th>
            </tr>
          </thead>
          <tbody id="table-body">
            {this.renderTableRows()}
          </tbody>
        </Table>
      </>
    );
  }
}

export default MainTable;