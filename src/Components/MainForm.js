import React, {Component} from 'react';
import {Alert, Button, Form} from "react-bootstrap";
import InputMask from 'react-input-mask';

class MainForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fullNameField: "",
      birthDateField: "",
      emailField: "",
      phoneField: "",
      distanceField: "",
      paymentField: "",
      dateRegistration: "",
      formValid: false,
      formBtnSubmitAttr: {
        disabled: true
      },
      formErrors: {
        email: ""
      }
    };

    this.handlerSubmit = this.handlerSubmit.bind(this);
    this.setBtnSubmitStatus = this.setBtnSubmitStatus.bind(this);
    this.state.dateRegistration = this.getFormattedDate(new Date());
  }

  clearForm() {
    this.setState({
      fullNameField: "",
      birthDateField: "",
      emailField: "",
      phoneField: "",
      distanceField: "",
      paymentField: "",
      formErrors: {
        email: "",
        toAllFields: ""
      },
      formValid: false,
      formBtnSubmitAttr: {
        disabled: true
      }
    })
  }

  handlerSubmit = (event) => {
    event.preventDefault();
    const name = this.state.fullNameField;
    const email = this.state.emailField;
    const distance = this.state.distanceField;
    const payment = this.state.paymentField;
    const id = this.props.membersList.length + 1;
    let date = this.state.birthDateField;
    let dateRegistration = this.state.dateRegistration;
    let phone = this.state.phoneField;

    date = this.getFormattedDate(date);
    phone =  phone ? "+" + phone.match(/\d]*/g).join("") : null;

    this.props.appendMemberToList({id, name, date, dateRegistration, email, phone, distance, payment});
    this.clearForm();
  };

  getFormattedDate(date) {
    const newDate = new Date(date);
    return newDate.getDate() + "." + (newDate.getMonth() + 1) + "." + newDate.getFullYear();
  }

  handlerFields = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({[name]: value});
  }

  setBtnSubmitStatus(e) {
    const focusedField = e.target;
    const focusedFieldName = focusedField.name;
    const cleanPhoneNumber = getCleanPhoneNumber(this.state.phoneField);
    let isAllFilled = true;
    let formError = {
      allFields: "",
      email: "",
      phone: ""
    };

    if (!this.state.fullNameField.length ||
      !this.state.birthDateField.length ||
      !this.state.emailField.length ||
      !this.state.phoneField.length ||
      !this.state.distanceField.length ||
      !this.state.paymentField.length
    ) {
      isAllFilled = false;
    } else {
      isAllFilled = true;
    }

    if (!isAllFilled) {
      formError.allFields = "Заполните все поля. ";
    }

    if (focusedFieldName === "emailField") {
      if (this.state.emailField) {
        function validateEmail(email) {
          // eslint-disable-next-line
          const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return re.test(String(email).toLowerCase());
        }

        if (!validateEmail(this.state.emailField)) {
          formError.email = "Неверно введен Email";
        }
      }
    }

    if (focusedFieldName === "phoneField") {
      if (cleanPhoneNumber.length !== 11) {
        formError.phone += "Завершите ввод номера телефона"
      }
    }

    function getCleanPhoneNumber(number) {
      return number ? number.match(/\d]*/g) : false;
    }

    this.setState({formErrors: {
        email: formError.email,
        phone: formError.phone,
        toAllFields: formError.allFields
      }});

    if (!formError.email && !formError.phone && !formError.allFields) {
      this.setState({
        formBtnSubmitAttr: {
          disabled: false
        }
      });
    } else {
      this.setState({
        formBtnSubmitAttr: {
          disabled: true
        }
      });
    }
  }

  render() {
    return (
      <>
        <Form id="formTable" onSubmit={this.handlerSubmit}>
          <Form.Group>
            <Form.Label>ФИО участника забега*</Form.Label>
            <Form.Control
              type="text"
              id="fullName"
              required
              name="fullNameField"
              value={this.state.fullNameField}
              onChange={this.handlerFields}
              onBlur={this.setBtnSubmitStatus}
            />
          </Form.Group>

          <Form.Group>
            <Form.Label>Дата рождения*</Form.Label>
            <Form.Control
              type="date"
              id="birthDate"
              required
              name="birthDateField"
              value={this.state.birthDateField}
              onChange={this.handlerFields}
              onBlur={this.setBtnSubmitStatus}
            />
          </Form.Group>

          <Form.Group>
            <Form.Label>Email*</Form.Label>
            <Form.Control
              type="email"
              id="email"
              required
              name="emailField"
              value={this.state.emailField}
              onChange={this.handlerFields}
              onBlur={this.setBtnSubmitStatus}
            />
            {this.state.formErrors.email ?
              <Alert className="mt-3" variant={"danger"}> {this.state.formErrors.email} </Alert> : ""}
          </Form.Group>

          <Form.Group>
            <Form.Label>Телефон*</Form.Label>
            <InputMask
              id="phone"
              mask="+7 (999) 999-99-99"
              maskChar={"_"}
              required
              name="phoneField"
              className="form-control"
              value={this.state.phoneField}
              onChange={this.handlerFields}
              onBlur={this.setBtnSubmitStatus}
            />
            {this.state.formErrors.phone ?
                <Alert className="mt-3" variant={"danger"}> {this.state.formErrors.phone} </Alert> : ""}
          </Form.Group>

          <Form.Group>
            <Form.Label>Дистанция*</Form.Label>
            <Form.Control
              as="select"
              id="distance"
              required
              name="distanceField"
              value={this.state.distanceField}
              onChange={this.handlerFields}
              onBlur={this.setBtnSubmitStatus}
            >
              <option value="" disabled>Выберите</option>
              <option value="3">3 км</option>
              <option value="3">5 км</option>
              <option value="10">10 км</option>
            </Form.Control>
          </Form.Group>

          <Form.Group>
            <Form.Label>Сумма взноса, руб.*</Form.Label>
            <InputMask
              id="payment"
              maskChar={""}
              mask="99999999999"
              required
              name="paymentField"
              className="form-control"
              value={this.state.paymentField}
              onChange={this.handlerFields}
              onBlur={this.setBtnSubmitStatus}
            />
          </Form.Group>

          <Form.Group>
            <Form.Label>Дата регистрации*</Form.Label>
            <Form.Control
              type="text"
              id="dateRegistration"
              readOnly
              value={this.state.dateRegistration}
            />
          </Form.Group>

            {this.state.formErrors.toAllFields ? <Alert variant={"danger"}> {this.state.formErrors.toAllFields} </Alert> : ""}

          <Button
            id="formBtnSubmit"
            variant="primary"
            disabled={this.state.formBtnSubmitAttr.disabled}
            block
            size="md"
            type="submit"
          >Отправить заявку</Button>
        </Form>
      </>
    );
  }
}

export default MainForm;