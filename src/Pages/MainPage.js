import React, {Component} from 'react';

import { Col, Container, Form, Row} from 'react-bootstrap';
import MainForm from "../Components/MainForm";
import MainTable from "../Components/MainTable";

class MainPage extends Component {
  constructor() {
    super();

    this.state = {
      sortBy: "dateRegistrationDown",
      members: []
    };

    this.appendMemberToList = this.appendMemberToList.bind(this);
    this.sortTableHandler = this.sortTableHandler.bind(this);
    this.sortTable = this.sortTable.bind(this);
  }

  componentDidMount() {
    this.getMembers();
  }

  getMembers() {
    fetch("/users.json")
    .then(res => res.json())
    .then(
      (result) => {
        this.setState({members: result.users});
        },
      (error) => { console.log("LoadFromJSON: " + error); }
    )
  }

  appendMemberToList(data) {
    this.setState({
      members: [...this.state.members, data]
    });
  }

  sortTableHandler(e) {
    this.setState({
      sortBy: e.target.value
    });
  }

  sortTable() {
    this.state.members.sort((a, b) => {
      switch (this.state.sortBy) {
        case "dateRegistrationDown":
          const aDateArrRegDown = a.dateRegistration.split(".");
          const bDateArrRegDown = b.dateRegistration.split(".");
          const aDateRegDown = new Date(aDateArrRegDown[2], aDateArrRegDown[1], aDateArrRegDown[0]);
          const bDateRegDown = new Date(bDateArrRegDown[2], bDateArrRegDown[1], bDateArrRegDown[0]);
          return bDateRegDown.getTime() > aDateRegDown.getTime() ? 1 : -1;
        case "dateRegistrationUp":
          const aDateArrRegUp = a.dateRegistration.split(".");
          const bDateArrRegUp = b.dateRegistration.split(".");
          const aDateRegUp = new Date(aDateArrRegUp[2], aDateArrRegUp[1], aDateArrRegUp[0]);
          const bDateRegUp = new Date(bDateArrRegUp[2], bDateArrRegUp[1], bDateArrRegUp[0]);
          return bDateRegUp.getTime() > aDateRegUp.getTime() ? -1 : 1;
        case "dateBirthDown":
          const aDateArrBirthDown = a.date.split(".");
          const bDateArrBirthDown = b.date.split(".");
          const aDateBirthDown = new Date(aDateArrBirthDown[2], aDateArrBirthDown[1], aDateArrBirthDown[0]);
          const bDateBirthDown = new Date(bDateArrBirthDown[2], bDateArrBirthDown[1], bDateArrBirthDown[0]);
          return bDateBirthDown.getTime() > aDateBirthDown.getTime() ? 1 : -1;
        case "dateBirthUp":
          const aDateArr2 = a.date.split(".");
          const bDateArr2 = b.date.split(".");
          const aDate2 = new Date(aDateArr2[2], aDateArr2[1], aDateArr2[0]);
          const bDate2 = new Date(bDateArr2[2], bDateArr2[1], bDateArr2[0]);
          return bDate2.getTime() > aDate2.getTime() ? -1 : 1;
        case "summDonationDown":
          return b.payment > a.payment ? 1 : -1 ;
        case "summDonationUp":
          return b.payment > a.payment ? -1 : 1 ;
        case "distanceDown":
          return b.distance > a.distance ? 1 : -1 ;
        case "distanceUp":
          return a.distance > b.distance ? 1 : -1 ;
        default:
          return false;
      }
    });
  }

  render() {
    return (
      <>
        <Container fluid style={{marginTop: "30px", maxWidth: "1500px"}}>
          <Row>
            <Col lg="3">
              <MainForm
                appendMemberToList={this.appendMemberToList}
                membersList={this.state.members}
              />
            </Col>

            <Col lg="9">
              <Form.Group controlId="sortTable" className="d-inline-block">

                <Form.Label>Сортировать по</Form.Label>
                <Form.Control
                  as="select"
                  value={this.state.sortBy}
                  onChange={this.sortTableHandler}
                >
                  <option value="dateRegistrationDown">Дата регистрации &#8595;</option>
                  <option value="dateRegistrationUp">Дата регистрации &#8593;</option>
                  <option value="dateBirthDown">Дата рождения &#8595;</option>
                  <option value="dateBirthUp">Дата рождения &#8593;</option>
                  <option value="summDonationDown">Сумма взноса &#8595;</option>
                  <option value="summDonationUp">Сумма взноса &#8593;</option>
                  <option value="distanceDown">Дистанция забега &#8595;</option>
                  <option value="distanceUp">Дистанция забега &#8593;</option>
                </Form.Control>
              </Form.Group>

              <MainTable membersList={this.state.members} sortTable={this.sortTable} />
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

export default MainPage;